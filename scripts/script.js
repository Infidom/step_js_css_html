let tab = function () {
  let tabTitle = document.querySelectorAll(".tabs-services-title"),
    tabContent = document.querySelectorAll(".tabs-services-item"),
    tabName;

  tabTitle.forEach((element) => {
    element.addEventListener("click", selectTabTitle);
  });

  function selectTabTitle() {
    tabTitle.forEach((element) => {
      element.classList.remove("active-tab");
    });

    this.classList.add("active-tab");
    tabName = this.getAttribute("data-tab");
    selectTabContent(tabName);
  }

  function selectTabContent(tabName) {
    tabContent.forEach((element) => {
      element.classList.contains(tabName)
        ? element.classList.add("active-content")
        : element.classList.remove("active-content");
    });
  }
};
tab();

let workList = document.querySelector(".our-work-title-list");
let workItem = document.querySelectorAll(".our-work-gallery-descr");
let loadMoreBtn = document.querySelector(".load-more");
let loader = document.querySelector(".loader");
let allImg = "all";
let cardsNum = 12;

function showLoader() {
  loadMoreBtn.classList.add("hide");
  loader.classList.remove("hide");
}
function hideLoader() {
  loadMoreBtn.classList.remove("hide");
  loader.classList.add("hide");
}

showCards();

loadMoreBtn.addEventListener("click", (event) => {
  event.preventDefault();

  showLoader();
  setTimeout(() => {
    cardsNum += 12;
    hideLoader();
    showCards(allImg);
  }, 2000);
});

workList.addEventListener("click", function (event) {
  let currentBtn = document.querySelector(".active");
  currentBtn.classList.remove("active");
  event.target.classList.add("active");

  let filterBtns = event.target.dataset.filter;
  cardsNum = 12;
  allImg = filterBtns;
  showCards(filterBtns);
});


function showCards(filter = "all") {
  let j = 0;

  for (let i = 0; i < workItem.length; i++) {
    if (
      (workItem[i].classList.contains(filter) || filter == "all") &&
      j < cardsNum
    ) {
      j++;
      workItem[i].classList.remove("hide");
    } else {
      workItem[i].classList.add("hide");
    }
  }

  if (j < cardsNum) {
    loadMoreBtn.classList.add("hide");
  } else {
    loadMoreBtn.classList.remove("hide");
  }
}


let slides = document.querySelectorAll(".slide-reviews");
let min = document.querySelectorAll(".min");
let leftArrow = document.querySelector(".left-arrow");
let rightArrow = document.querySelector(".right-arrow");

let currentSlide = 0;

window.onload = function () {
  function playSlide(slide_reviews) {
    for (let k = 0; k < min.length; k++) {
      slides[k].classList.remove("active-review");
      min[k].classList.remove("active-review");
    }

    if (slide_reviews < 0) {
      slide_reviews = currentSlide = slides.length - 1;
    }

    if (slide_reviews > slides.length - 1) {
      slide_reviews = currentSlide = 0;
    }

    slides[slide_reviews].classList.add("active-review");
    min[slide_reviews].classList.add("active-review");
  }

  leftArrow.addEventListener("click", function () {
    playSlide((currentSlide -= 1));
  });

  rightArrow.addEventListener("click", function () {
    playSlide((currentSlide += 1));
  });

  for (let l = 0; l < min.length; l++) {
    min[l].addEventListener("click", function () {
      playSlide((currentSlide = Array.from(min).indexOf(this)));
    });
  }

  playSlide(currentSlide);
};
